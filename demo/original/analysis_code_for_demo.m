
load('LUT_intra.mat')
clip_data_flag = 0;
for LUT_intra_row_idx = 1
    
    
    LUT_intra_row_idx
    cd ../data/raw
    cd(string(LUT_intra_demo.expt_date(LUT_intra_row_idx)))
    expt_date = LUT_intra_demo.expt_date(LUT_intra_row_idx);
    filename = LUT_intra_demo.filename(LUT_intra_row_idx);
    
    % filename = "M1_N3_T5"; %change HES parameters if required.
    filename_str = sprintf("%s.nwb", filename);
    nwb_in = nwbRead(filename_str);
    
    % disp(nwb_in);
    rec = nwb_in.acquisition.get('response_to_JO_stimulation');
    data = rec.data.load;
    time = rec.timestamps.load;
    
    row_name = strcat(string(expt_date), "_",filename);
    
    parameters = LUT_intra_demo(row_name,:);
    
    
    ON_dur = parameters.ON_dur;
    OFF_dur =parameters.OFF_dur;
    no_of_trials = parameters.no_of_trials;
    fs = parameters.fs;
    
    max_chirp_frq = parameters.max_chirp_frq;
    amp_sweep_frq = parameters.amp_sweep_frq;
    blwgn_fc = parameters.blwgn_fc;
    impulse_dur = ON_dur;
    
    

    
    start_stim = OFF_dur*fs;
    stop_stim = (ON_dur+OFF_dur)*fs;
    
    single_trial_length =start_stim + stop_stim+1;
 
    
    % Run this to take stimulus order from nwb file stimulus description
    
    if parameters.stim_order == ""
        stim = nwb_in.stimulus_presentation.get('mechanical_stimulus');
        %         stim_order = stim.stimulus_description;
        stim_order_vector = string(split(stim.stimulus_description, ','));
    else
        stim_order = parameters.stim_order;
        stim_order_vector = split(parameters.stim_order, ',');
    end
    
    
    
    [stim_order_sorted,idx] = sort(stim_order_vector);

    no_of_protocols = length(unique(stim_order_sorted));
    valid_trials = round(length(data)/single_trial_length);
    
    rec_data = data(:,1);
    stim_fb = data(:,2);
    hes_data = data(:,3);
    
    rec_fc_low = 10;
    rec_fc_high = 2000;
    rec_filt_order = 8;
    d_rec = designfilt('bandpassiir','FilterOrder',rec_filt_order, ...
        'HalfPowerFrequency1',rec_fc_low,'HalfPowerFrequency2',rec_fc_high, ...
        'SampleRate',fs, 'DesignMethod', 'butter');
    % fvtool(d_rec);
    
    filtered_data_bp = filtfilt(d_rec, rec_data);
    
    
    %
    
    fig_handle = consolidated_plot(time, filtered_data_bp, hes_data, stim_fb, fs);
    

    rec_protocols_reshaped = reshape_data(filtered_data_bp, single_trial_length, no_of_protocols, valid_trials);
    stim_protocols_hes_reshaped = reshape_data(hes_data, single_trial_length, no_of_protocols, valid_trials); %hes data not filtered. Antennal movement not calculated
    stim_protocols_ifb_reshaped = reshape_data(stim_fb, single_trial_length, no_of_protocols, valid_trials);
    
    % sort reshaped data

    rec_protocols_sorted = rec_protocols_reshaped(idx,:);
    stim_protocols_hes_sorted = stim_protocols_hes_reshaped(idx,:); %hes data not filtered. Antennal movement not calculated
    stim_protocols_ifb_sorted = stim_protocols_ifb_reshaped(idx,:);
    
    % Create structs
    
    d_ref = datetime('2021.06.01', 'InputFormat', 'yyyy.MM.dd');
    d_check = datetime(expt_date, 'InputFormat', 'yyyy.MM.dd');
    
    a= .9258; b=93.15; c=-1.455; %before 1 June 2021
       
    P = create_structs(rec_protocols_sorted,stim_protocols_hes_sorted,fs, stim_protocols_ifb_sorted, no_of_protocols, no_of_trials, single_trial_length, stim_order_sorted,max_chirp_frq, amp_sweep_frq, blwgn_fc, ON_dur, a, b, c);

    
    [P(:).ON_dur] = deal(ON_dur);
    [P(:).OFF_dur] = deal(OFF_dur);
    [P(:).time] = deal(time);
    [P(:).filename] = deal(filename);
    [P(:).date] = deal(expt_date);
    [P(:).no_of_trials] = deal(no_of_trials);
    [P(:).fs] = deal(fs);
    [P(:).no_of_protocols] = deal(no_of_protocols);
    [P(:).single_trial_length] = deal(single_trial_length);
    
    for i=1:no_of_protocols
        gcfr_max = max(P(i).gcfr(:));
        P(i).norm_gcfr = P(i).gcfr/gcfr_max;
        
    end
    
    % Plot data
    
    plot_data(single_trial_length,no_of_protocols, fs, time, filename,  P);

    
    % GCFR Vs frequency and spike phase Vs Frequency
    
    for i = 1:no_of_protocols
        if P(i).stim_type == "frq"  
            
            P(i).inc_frq_chirp_f = linspace(1,max_chirp_frq,ON_dur*fs+1);
            [I_spike_phase, II_spike_phase, III_spike_phase, I_spike_freq, II_spike_freq, III_spike_freq] = spike_phase(P(i).antennal_movement(1,:), P(i).raster(1,:), fs, start_stim, stop_stim, P(i).stim_type, P(i).inc_frq_chirp_f);
            figure(); scatter(I_spike_freq,I_spike_phase,100, '.k'); hold on;
            pmin = min([I_spike_phase II_spike_phase III_spike_phase]);
            pmax = max([I_spike_phase II_spike_phase III_spike_phase]);
            pimin = floor(pmin/pi);
            pimax = ceil(pmax/pi);
            ylim([pimin pimax]);
            yticks(pimin:pimax/4:pimax)
            yticklabels( string(pimin:pimax/4:pimax) + "\pi" )
            scatter(II_spike_freq,II_spike_phase, 100,  'b.');
            scatter(III_spike_freq,III_spike_phase, 100, 'r.');
            title('Frequency chirp');
            ylabel('Spike phase (rad)');
            xlabel('Stimulus frequency (Hz)');
            box off;
            legend('I spike','II spike', 'III spike', 'Location', 'best');
        end
        
        if P(i).stim_type == "frq"
            
            P(i).I_spike = [I_spike_freq', I_spike_phase'];
            P(i).II_spike = [II_spike_freq', II_spike_phase'];
            P(i).III_spike = [III_spike_freq', III_spike_phase'];
            
            P(i).inc_frq_chirp_f = linspace(1,max_chirp_frq,ON_dur*fs+1);
            figure;
            [lineOut, ~] = stdshade(P(i).norm_gcfr(:,start_stim:stop_stim),0.2,'k',P(i).inc_frq_chirp_f);
            lineOut.LineWidth  = 0.01;
            ylabel 'GCFR';
            xlabel 'Frequency (Hz)';
            title ('Response to increasing frequency chirp');
            box off;
            
        end
    end
    

    
    
end

