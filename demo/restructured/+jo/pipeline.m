function pipeline(dataDirectory, fileId)
%PIPELINE Perform the process -> analyze -> plot pipeline for fileId 
    jo.data.process(dataDirectory, fileId);
    jo.data.analyze(dataDirectory, fileId);
end

