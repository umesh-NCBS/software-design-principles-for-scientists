function spikesData = spikes(filteredMembranePotential, minSpikeHeight)
%GETSPIKES 
    dataSize = size(filteredMembranePotential);
    spikesData = zeros(dataSize);
    for iTrial = 1:(dataSize(2))
        [~, spikePositions] = findpeaks(filteredMembranePotential(:,iTrial), "MinPeakHeight", minSpikeHeight(iTrial));
        spikesData(spikePositions,iTrial) = 1;
    end
end

