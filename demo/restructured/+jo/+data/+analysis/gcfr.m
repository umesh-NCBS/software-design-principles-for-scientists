function gcfrData = gcfr(spikes, sigma, samplingRate)
%GCFR Summary of this function goes here
%   Detailed explanation goes here
    alpha = 2.5;
    windowLength = ceil((sigma * 2 * alpha) + 1);
    gaussianWindow = gausswin(windowLength, alpha);
    gcfrData = filter(gaussianWindow, sum(gaussianWindow), spikes) * samplingRate;
end

