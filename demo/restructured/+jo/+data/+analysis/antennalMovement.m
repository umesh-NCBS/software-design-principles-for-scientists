function antennalMovementData = antennalMovement(hallEffectSensorData, hallParameters)
%ANTENNALMOVEMENT 
    antennalMovementData = ((hallParameters.b ./ (hallEffectSensorData - hallParameters.a)) .^ (1/3)) + hallParameters.c;
end

