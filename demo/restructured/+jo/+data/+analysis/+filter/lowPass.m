function filteredData = lowPass(data, frequency, samplingRate, order)
%LOWPASSFILTER 
    digitalFilter = designfilt(...
        'lowpassiir', 'FilterOrder', order, 'SampleRate', samplingRate, ...
        'HalfPowerFrequency', frequency, ...
        'DesignMethod', 'butter');
    filteredData = filtfilt(digitalFilter, data);
end


