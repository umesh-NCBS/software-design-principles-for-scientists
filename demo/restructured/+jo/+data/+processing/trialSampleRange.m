function trialSampleRangeData = trialSampleRange(parameters, acquisitionDataLength)
%TRIALEXTENT
    nSamplesInTrial = ((parameters.OFF_dur + parameters.ON_dur + parameters.OFF_dur) * parameters.fs) + 1;
    stimulusOrder = split(parameters.stim_order, ',');
    nCompletedTrials = round(floor(acquisitionDataLength / nSamplesInTrial));
    
    for iTrial = 1:nCompletedTrials
        trialSampleRangeData(iTrial).stimulusType = stimulusOrder(iTrial);
        trialSampleRangeData(iTrial).sampleRange = [(((iTrial - 1) * nSamplesInTrial) + 1) (iTrial * nSamplesInTrial)];
        %class(trialSampleRangeData(iTrial).sampleRange)
    end
end

