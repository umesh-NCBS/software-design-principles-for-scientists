function sortedStimulusData = sortTrials(timestamps, stimulusData, acquisitionData, trialSampleRangeData)
%SORTTRIALS 
    membranePotentialDataFull = acquisitionData(:,1);
    stimulusForceFeedbackDataFull = acquisitionData(:,2);
    hallEffectSensorDataFull = acquisitionData(:,3);
    
    sortedStimulusData = struct;
    
    for iTrialAll = 1:length(trialSampleRangeData)
        stimulusType = trialSampleRangeData(iTrialAll).stimulusType;
        sampleRange = trialSampleRangeData(iTrialAll).sampleRange;
        
        trialTimestamps = timestamps(sampleRange(1):sampleRange(2));
        intendedStimulus = stimulusData(:,iTrialAll);
        membranePotential = membranePotentialDataFull(sampleRange(1):sampleRange(2));
        stimulusForceFeedback = stimulusForceFeedbackDataFull(sampleRange(1):sampleRange(2));
        hallEffectSensor = hallEffectSensorDataFull(sampleRange(1):sampleRange(2));
        
        if isfield(sortedStimulusData, stimulusType)
            sortedStimulusData.(stimulusType).timestamps = cat(2, sortedStimulusData.(stimulusType).timestamps, trialTimestamps);
            sortedStimulusData.(stimulusType).intendedStimulus = cat(2, sortedStimulusData.(stimulusType).intendedStimulus, intendedStimulus);
            sortedStimulusData.(stimulusType).membranePotential = cat(2, sortedStimulusData.(stimulusType).membranePotential, membranePotential);
            sortedStimulusData.(stimulusType).stimulusForceFeedback = cat(2, sortedStimulusData.(stimulusType).stimulusForceFeedback, stimulusForceFeedback);
            sortedStimulusData.(stimulusType).hallEffectSensor = cat(2, sortedStimulusData.(stimulusType).hallEffectSensor, hallEffectSensor);
            
        else
            sortedStimulusData.(stimulusType).timestamps = trialTimestamps;
            sortedStimulusData.(stimulusType).intendedStimulus = intendedStimulus;
            sortedStimulusData.(stimulusType).membranePotential = membranePotential;
            sortedStimulusData.(stimulusType).stimulusForceFeedback = stimulusForceFeedback;
            sortedStimulusData.(stimulusType).hallEffectSensor = hallEffectSensor;
        end
    end
end

