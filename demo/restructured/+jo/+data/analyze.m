function analyze(dataDirectory, fileId)
%ANALYZE 
    MEMBRANE_POTENTIAL_BANDPASS_FREQUENCY = [10, 2000]; % Hz
    MEMBRANE_POTENTIAL_BANDPASS_ORDER = 6;
    SPIKE_HEIGHT_PROPORTION = 0.25;
    GCFR_SIGMA = 200; % 10 ms
    ANTENNAL_MOVEMENT_LOWPASS_FREQUENCY = 600; % Hz
    ANTENNAL_MOVEMENT_LOWPASS_ORDER = 6;
    
    analyzedData = struct;
    stimuli = jo.io.processed.getStimuli(dataDirectory, fileId);
    for iStimulus = 1:length(stimuli)
        stimulusName = stimuli{iStimulus};
        analyzedData.(stimulusName) = struct;
        [stimulusParameters, stimulusData] = jo.io.processed.readStimulus(...
            dataDirectory, fileId, stimulusName);
        filteredMembranePotential = jo.data.analysis.filter.bandPass(...
            100 * stimulusData.membranePotential, MEMBRANE_POTENTIAL_BANDPASS_FREQUENCY,...
            stimulusParameters.samplingRate, MEMBRANE_POTENTIAL_BANDPASS_ORDER);
        analyzedData.(stimulusName).filteredMembranePotential = filteredMembranePotential;
        analyzedData.(stimulusName).spikes = jo.data.analysis.spikes(...
            filteredMembranePotential, SPIKE_HEIGHT_PROPORTION * max(filteredMembranePotential));
        analyzedData.(stimulusName).gcfr = jo.data.analysis.gcfr(...
            analyzedData.(stimulusName).spikes, GCFR_SIGMA, stimulusParameters.samplingRate);
        antennalMovement = jo.data.analysis.antennalMovement(...
            stimulusData.hallEffectSensor, stimulusParameters.hallParameters);
        analyzedData.(stimulusName).filteredAntennalMovement = jo.data.analysis.filter.lowPass(...
            antennalMovement, ANTENNAL_MOVEMENT_LOWPASS_FREQUENCY, ...
            stimulusParameters.samplingRate, ANTENNAL_MOVEMENT_LOWPASS_ORDER);
        analyzedData.(stimulusName).samplingRate = stimulusParameters.samplingRate;
    end
    parameters = struct;
    parameters.membranePotentialBandpassFrequency = MEMBRANE_POTENTIAL_BANDPASS_FREQUENCY;
    parameters.membranePotentialBandpassOrder = MEMBRANE_POTENTIAL_BANDPASS_ORDER;
    parameters.spikeHeightProportion = SPIKE_HEIGHT_PROPORTION;
    parameters.gcfrSigma = GCFR_SIGMA;
    parameters.antennalMovementLowpassFrequency = ANTENNAL_MOVEMENT_LOWPASS_FREQUENCY;
    parameters.antennalMovementLowpassOrder = ANTENNAL_MOVEMENT_LOWPASS_ORDER;
    
    jo.io.analyzed.write(dataDirectory, fileId, parameters, analyzedData)
end

