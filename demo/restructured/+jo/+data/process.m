function process(dataDirectory, fileId)
%PROCESS Process raw data for further analysis
    
    [timestamps, stimulusData, acquisitionData] = jo.io.raw.read(dataDirectory, fileId);
    parameters = jo.io.raw.getParameters(dataDirectory, fileId);
    
    trialSampleRangeData = jo.data.processing.trialSampleRange(parameters, length(acquisitionData));
    sortedStimulusData = jo.data.processing.sortTrials(timestamps, stimulusData, acquisitionData, trialSampleRangeData);
    
    jo.io.processed.write(dataDirectory, fileId, parameters, sortedStimulusData);
end

