function write(dataDirectory, fileId, parameters, sortedStimulusData)
%SAVEPROCESSED 
    processedFilePath = [dataDirectory, '/processed/', regexprep(fileId, '_', '/', 1), '.hdf5'];
    [processedFileLocation, ~, ~] = fileparts(processedFilePath);
    [~, ~] = mkdir(processedFileLocation);
    delete(processedFilePath);    
    
    stimulusTypes = fieldnames(sortedStimulusData);
    for iStimulusType = 1:length(stimulusTypes)
        stimulusName = stimulusTypes{iStimulusType};
        writeDataset = @(datasetName, unit) jo.io.helpers.writeDataset(processedFilePath, stimulusName, sortedStimulusData.(stimulusName), datasetName, unit);
        writeDataset('timestamps', 's');
        writeDataset('intendedStimulus', 'V');
        writeDataset('stimulusForceFeedback', 'V');
        writeDataset('hallEffectSensor', 'V');
        writeDataset('membranePotential', '10 V');
        stimulusGroupPath = ['/', stimulusName];
        writeAttribute = @(attribute, value) h5writeatt(processedFilePath, stimulusGroupPath, attribute, value);
        writeAttribute('Sampling rate', parameters.fs);
        writeAttribute('On duration', parameters.ON_dur);
        writeAttribute('Off duration', parameters.OFF_dur);
        writeAttribute('Hall-effect sensor a', parameters.hallParameters.a);
        writeAttribute('Hall-effect sensor b', parameters.hallParameters.b);
        writeAttribute('Hall-effect sensor c', parameters.hallParameters.c);
        switch stimulusName
            case 'frq_chirp'
                writeAttribute('Max chirp frequency', parameters.max_chirp_frq);
            case 'amp_sweep'
                writeAttribute('Amplitude sweep frequency', parameters.amp_sweep_frq);
            case 'blwgn'
                writeAttribute('BLWGN cutoff frequency', parameters.blwgn_fc);
        end
    end
    h5writeatt(processedFilePath, '/', 'JO file type', 'processed');
    h5writeatt(processedFilePath, '/', 'file ID', fileId);
    h5writeatt(processedFilePath, '/', 'Stimulus order', parameters.stim_order);
end