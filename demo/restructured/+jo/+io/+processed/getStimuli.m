function stimuli = getStimuli(dataDirectory, fileId)
%GETSTIMULI 
    stimuli = arrayfun(@(group)(strip(string(group.Name), '/')), ...
        h5info([dataDirectory, '/processed/', regexprep(fileId, '_', '/', 1), '.hdf5']).Groups);
end

