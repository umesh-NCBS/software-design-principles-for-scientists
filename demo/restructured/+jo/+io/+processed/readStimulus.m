function [parameters, data] = readStimulus(dataDirectory, fileId, stimulusName)
%READSTIMULUS 
    analyzedFilePath = [dataDirectory, '/processed/', regexprep(fileId, '_', '/', 1), '.hdf5'];
    
    parameters = struct;
    data = struct;
    
    datasetNames = {'timestamps', 'intendedStimulus', 'stimulusForceFeedback', 'hallEffectSensor', 'membranePotential'};
    stimulusGroupPath = ['/', stimulusName];
    for iDataset = 1:length(datasetNames)
        datasetName = datasetNames{iDataset};
        data.(datasetName) = h5read(analyzedFilePath, [stimulusGroupPath, '/', datasetName]);
    end
    parameters.samplingRate = h5readatt(analyzedFilePath, stimulusGroupPath, 'Sampling rate');
    parameters.onDuration = h5readatt(analyzedFilePath, stimulusGroupPath, 'On duration');
    parameters.offDuration = h5readatt(analyzedFilePath, stimulusGroupPath, 'Off duration');
    parameters.hallParameters.a = h5readatt(analyzedFilePath, stimulusGroupPath, 'Hall-effect sensor a');
    parameters.hallParameters.b = h5readatt(analyzedFilePath, stimulusGroupPath, 'Hall-effect sensor b');
    parameters.hallParameters.c = h5readatt(analyzedFilePath, stimulusGroupPath, 'Hall-effect sensor c');
    switch stimulusName
        case 'frq_chirp'
            parameters.maxChirpFrequency = h5readatt(analyzedFilePath, stimulusGroupPath, 'Max chirp frequency');
        case 'amp_sweep'
            parameters.amplitudeSweepFrequency = h5readatt(analyzedFilePath, stimulusGroupPath, 'Amplitude sweep frequency');
        case 'blwgn'
            parameters.blwgnCutoffFrequency = h5readatt(analyzedFilePath, stimulusGroupPath, 'BLWGN cutoff frequency');
    end
end

