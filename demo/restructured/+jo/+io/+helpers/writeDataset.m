function writeDataset(filePath, stimulusName, stimulusData, datasetName, unit)
%WRITEDATASET 
    datasetParameters = {'Shuffle', 1, 'Deflate', 9, 'Fletcher32', 1, 'ChunkSize', [100, 1]};
    datasetPath = ['/', stimulusName, '/', datasetName];
    data = stimulusData.(datasetName);
    h5create(filePath, datasetPath, size(data), datasetParameters{:});
    h5write(filePath, datasetPath, data);
    h5writeatt(filePath, datasetPath, 'Unit', unit);
end

