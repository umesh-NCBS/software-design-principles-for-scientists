function [timestamps, stimulusData, acquisitionData] = read(dataDirectory, fileId)
%LOAD Load acquisition and stimulus data with timestamps from raw NWB file
%   
    nwbFile = nwbRead([dataDirectory, '/raw/', regexprep(fileId, '_', '/', 1), '.nwb']);
    acquisition = nwbFile.acquisition.get('response_to_JO_stimulation');
    timestamps = acquisition.timestamps.load;
    acquisitionData = acquisition.data.load;
    stimulusData = nwbFile.stimulus_presentation.get('mechanical_stimulus').data.load;
end