function parameters = getParameters(dataDirectory, fileId)
%GETPARAMETERS Summary of this function goes here
%   Detailed explanation goes here
    
    load([dataDirectory, '/LUT_intra.mat'], 'LUT_intra');
    
    parameters = LUT_intra(fileId, :);
    
    if parameters.stim_order == ""
        nwbFile = nwbRead([dataDirectory, '/raw/', parameters.expt_date, '/', parameters.filename, '.nwb']);
        mechanicalStimulus = nwbFile.stimulus_presentation.get('mechanical_stimulus');
        parameters.stim_order = mechanicalStimulus.stimulus_description;
    end
    
    load([dataDirectory, '/hallParameters.mat'], 'hallParameters');
    parameters.hallParameters = hallParameters.pre202106;
end

