function [parameters, data] = readStimulus(dataDirectory, fileId, stimulusName)
%READSTIMULUS 
    processedFilePath = [dataDirectory, '/processed/', regexprep(fileId, '_', '/', 1), '.hdf5'];
    
    parameters = struct;
    data = struct;
    
    datasetNames = {'timestamps', 'intendedStimulus', 'stimulusForceFeedback', 'hallEffectSensor', 'membranePotential'};
    stimulusGroupPath = ['/', stimulusName];
    for iDataset = 1:length(datasetNames)
        datasetName = datasetNames{iDataset};
        data.(datasetName) = h5read(processedFilePath, [stimulusGroupPath, '/', datasetName]);
    end
    parameters.samplingRate = h5readatt(processedFilePath, stimulusGroupPath, 'Sampling rate');
    parameters.onDuration = h5readatt(processedFilePath, stimulusGroupPath, 'On duration');
    parameters.offDuration = h5readatt(processedFilePath, stimulusGroupPath, 'Off duration');
    parameters.hallParameters.a = h5readatt(processedFilePath, stimulusGroupPath, 'Hall-effect sensor a');
    parameters.hallParameters.b = h5readatt(processedFilePath, stimulusGroupPath, 'Hall-effect sensor b');
    parameters.hallParameters.c = h5readatt(processedFilePath, stimulusGroupPath, 'Hall-effect sensor c');
    switch stimulusName
        case 'frq_chirp'
            parameters.maxChirpFrequency = h5readatt(processedFilePath, stimulusGroupPath, 'Max chirp frequency');
        case 'amp_sweep'
            parameters.amplitudeSweepFrequency = h5readatt(processedFilePath, stimulusGroupPath, 'Amplitude sweep frequency');
        case 'blwgn'
            parameters.blwgnCutoffFrequency = h5readatt(processedFilePath, stimulusGroupPath, 'BLWGN cutoff frequency');
    end
end

