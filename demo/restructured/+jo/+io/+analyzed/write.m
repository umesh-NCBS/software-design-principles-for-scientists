function write(dataDirectory, fileId, parameters, analyzedData)
%WRITESTIMULUS
    analyzedFilePath = [dataDirectory, '/analyzed/', regexprep(fileId, '_', '/', 1), '.hdf5'];
    [analyzedFileLocation, ~, ~] = fileparts(analyzedFilePath);
    [~, ~] = mkdir(analyzedFileLocation);
    delete(analyzedFilePath);
    
    stimulusTypes = fieldnames(analyzedData);
    for iStimulusType = 1:length(stimulusTypes)
        stimulusName = stimulusTypes{iStimulusType};
        writeDataset = @(datasetName, unit) jo.io.helpers.writeDataset(analyzedFilePath, stimulusName, analyzedData.(stimulusName), datasetName, unit);
        writeDataset('filteredMembranePotential', 'mV');
        writeDataset('spikes', "");
        writeDataset('gcfr', 'Hz');
        writeDataset('filteredAntennalMovement', 'mm');
        h5writeatt(analyzedFilePath, ['/', stimulusName], 'samplingRate', analyzedData.(stimulusName).samplingRate);
    end
    writeAttribute = @(attribute, value) h5writeatt(analyzedFilePath, '/', attribute, value);
    writeAttribute('Membrane potential bandpass frequency', parameters.membranePotentialBandpassFrequency);
    writeAttribute('Membrane potential bandpass order', parameters.membranePotentialBandpassOrder);
    writeAttribute('Spike height proportion', parameters.spikeHeightProportion);
    writeAttribute('GCFR sigma', parameters.gcfrSigma);
    writeAttribute('Antennal movement lowpass frequency', parameters.antennalMovementLowpassFrequency);
    writeAttribute('Antennal movement lowpass order', parameters.antennalMovementLowpassOrder);
    writeAttribute('JO file type', 'analyzed');
    writeAttribute('file ID', fileId);
end

